master: [![build status](https://gitlab.com/Mizux/test/badges/master/build.svg)](https://gitlab.com/Mizux/test/commits/master)  
[TOC]
# Introduction
POC about Gitlab-CI (i.e. *.gitlab-ci.yml*) running locally.

The goal is:
* Build a docker image containing the environment (e.g. 
compiler, cross toolchain etc...).
* Then reuse this image to build the project.
* Then reuse this image and output of the build to test the project.

`docker -> build -> test`

# Host Dependencies
First, to run pipeline locally you'll need:  
* A working docker daemon running.
* gitlab-runner program available.

# Passing Data between jobs
Supposing you have the pipeline `job1 -> job2`, and you reuse data produced by
job1 in job2.  
Then you can use the [cache](https://docs.gitlab.com/ce/ci/yaml/README.html#cache) feature of gitlab-ci !

Here a .gitlab-ci.yml snippet:
```yml
stages:
  - one
  - two

job1:
  stage: one
  image: ...
  script:
    - echo "Hello World" > file.txt
  cache:
    key: $CI_COMMIT_REF_NAME
    paths:
      - file.txt
    policy: push

job2:
  stage: two
  image: ...
  script:
    - cat file.txt
  cache:
    key: $CI_COMMIT_REF_NAME
    paths:
      - file.txt
    policy: pull
```

**warning:** By default each job has a separate cache so we need to use a key so
job1 and job2 use the same cache.  
note: here cache policy are optional.  
note: It's seems that `$CI_COMMIT_REF_SHA` is empty with gitlab-runner.  
note: If you specify several caches, only the last one will be take into account.

## Running the pipeline locally
To run the pipeline locally you'll need:
1. Create a temporary directory to store the cache,
2. Then call the jobs sequentially with cache in the docker container specified (i.e. `--cache-dir=container_path`) and directory mounted in the
docker container (i.e. `--docker-volumes host_path:container_path`).

```sh
mkdir -v cache
gitlab-runner exec docker --cache-dir=/cache --docker-volumes $(pwd)/cache:/cache job1
gitlab-runner exec docker --cache-dir=/cache --docker-volumes $(pwd)/cache:/cache job2
sudo rm -rf cache
```

note: by default gitlab-runner docker will create cache with root user thus we need root
permission to remove it (see below to fix it).  

In detail, In fact each `gitlab-runner` call will spawn a new container, so if we want to be
able to pass cache from one job to another, we need to store it on the host machine and then mount it in the container.  

## Remove sudo clean
To avoid the *sudo clean* at the end, you can add a clean job in the `.gitlab-ci.yml`.
```yml
stages:
  ...
  - clean

clean:
  stage: clean
  image: docker:latest
  script:
    - (cd /cache && rm -rf *) || true

```

Then locally:
```sh
...
gitlab-runner exec docker \
  --cache-dir=/cache --docker-volumes $(pwd)/cache:/cache \
  clean

rmdir cache

```


# Building Custom Image
To build image in a job you'll need a docker image and the DinD (Docker in Docker) service.

```yml
docker:
  stage: docker
  image: docker:latest
  services:
    - docker:dind
  script:
    - docker build -t image:latest .
    - docker save image:latest -o image.tar
  cache:
    paths:
      - image.tar
```
note:Usually you can use the cache flag `policy: push` since image build is the first
step of the pipeline.  
note: `docker -v /var/run/docker.sock:/var/run/docker.sock` [approach](https://docs.gitlab.com/ce/ci/docker/using_docker_build.html#use-docker-socket-binding) is not possible on gitlab.com

## Using Docker Image
Once image has been built and push in the cache, it's time to use it in the
following jobs.

```yml
build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - docker load -i image.tar
    - docker run --rm -v $(pwd):/project image:latest /bin/sh -c "cd /project && ..."
  cache:
    paths:
      - image.tar
```

Since we run a new container, we must "bind" the project directory into the new
container by using `-v $(pwd):/project`.  
**warning:** Don't forget to jump into the project folder inside the container
i.e. `cd /project`

## Running job locally
In order to be able to have Docker in Docker running locally, you'll need to
give `privileged` access to docker container.

```sh
mkdir -v cache
gitlab-runner exec docker \
  --cache-dir=/cache --docker-volumes $(pwd)/cache:/cache \
  --docker-privileged \
  docker
gitlab-runner exec docker \
  --cache-dir=/cache --docker-volumes $(pwd)/cache:/cache \
  --docker-privileged \
  build
sudo rm -rf cache
```

# Cleaning Local Docker
If you interrupt a local pipeline (e.g. SIGTERM) you can have dead containers and volumes remaining.  
To clean them all:
```sh
docker rm (docker ps -aq)
docker volume prune
```

**warning:** This will clean any containers on the host.
