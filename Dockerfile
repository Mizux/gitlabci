FROM alpine:latest
LABEL maintainer="dev@mizux.net"

ADD setup.sh .
RUN ./setup.sh
