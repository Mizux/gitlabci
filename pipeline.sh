#! /usr/bin/env bash

set -x
mkdir -v cache

gitlab-runner exec docker \
	--cache-dir=/cache --docker-volumes $(pwd)/cache:/cache \
	--docker-privileged \
	docker

gitlab-runner exec docker \
	--cache-dir=/cache --docker-volumes $(pwd)/cache:/cache \
	--docker-privileged \
	build

gitlab-runner exec docker \
	--cache-dir=/cache --docker-volumes $(pwd)/cache:/cache \
	--docker-privileged \
	test

sudo tree cache
sudo find . -type f -iname "*.zip" -exec unzip -l {} \;

gitlab-runner exec docker \
	--cache-dir=/cache --docker-volumes $(pwd)/cache:/cache \
	clean

sudo tree cache
sudo find . -type f -iname "*.zip" -exec unzip -l {} \;
rmdir cache

